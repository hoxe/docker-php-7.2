## Setting up Docker

### Change host name of database server

The database is SQL Server, so we need to update the host name in some configuration files:

- ./../configuration/config.connection.json
- ./../configuration/log4php_config.xml

### Choose an available port for your application.
I am used the "8080" port on my local machine because it is suitable with me, but you do not need to follow the port.
You could change to another available port for your machine:
```
    8080:80 -> 80:80
```

### Update domain

Because of using Docker for web server, we need to add a domain mapping on your local machine.

- Add below line to `/etc/hosts` (your local machine):
```
    127.0.0.1   dev.comet.ws.dgq
```

#### Run Containers
We used `docker-composer` to work with docker. First off all, we need to go to the docker directory.
`path: ./docker`

There are 3 common steps:

```
// 1. Build docker (for the first time)
    docker-compose build
// if there is any problems when you running your first build. You should change the command like below
    docker-compose up -d --build
    

// 2. Start docker containers.
    docker-compose up -d

// 3. Stop docker containers.
    docker-compose down
```

### Import database for Database container

After starting the db container for the first time, we need to import database (detail guideline can be found in [here](http://wiki.laudert.intra/display/PRINT/PHP+Projects))


### Debug with xDebug and IntelliJ IDEA/phpStorm

Almost of configurations for docker web server have been prepared already. One thing you need to do is configure your local ip address as an environment variable (add `.env` file).

```
XDEBUG_HOST_IP={YOUR_IP_ADDRESSS}
```   

[Reference](https://www.arroyolabs.com/2016/10/docker-xdebug/);